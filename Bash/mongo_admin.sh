#!/bin/bash -eu

PS3='Please select a task: '
options=("Change password" "Add user" "Remove user" "Grant role to user" "Revoke role from user" "Print database users" "Quit")

cacert="./cacert.pem"
dbhost="scs-mongodb.scc.kit.edu"

echo "Administration tasks on" $dbhost

read -p "x509 authentication [no/yes] (press Enter if you don't know): " x509_auth
if [[ $x509_auth == "yes" ]]; then
  read -p "Your certificate/key file: " certkeyfile
  connect="mongo --tls --tlsCAFile $cacert --host $dbhost --tlsCertificateKeyFile $certkeyfile --authenticationDatabase \$external --authenticationMechanism MONGODB-X509 --eval"
else
  read -p "Your authentication database: " authdb
  read -p "Your username: " username
  connect="mongo --tls --tlsCAFile $cacert --host $dbhost --authenticationDatabase $authdb --username $username --password --eval"
fi

select opt in "${options[@]}"
do
  case $opt in
    "Change password")
      echo "Changing a password"
      break;;
    "Add user")
      echo "Adding a new user"
      break;;
    "Remove user")
      echo "Removing a user"
      break;;
    "Grant role to user")
      echo "Granting a role"
      break;;
    "Revoke role from user")
      echo "Revoking a role"
      break;;
    "Print database users")
      echo "Printing database users"
      break;;
    "Quit")
      echo "Quitting"
      exit;;
    *)
      echo "invalid option $REPLY";;
  esac
done

if [[ $opt == "Change password" ]]; then
  read -p "Authentication database of the user: " dbname
  read -p "Username for which password will be changed: " other_user
  $connect "db.changeUserPassword(\"$other_user\", passwordPrompt())" $dbname
  status=$?
  if [[ $status -ne 0 ]]; then
    echo "Error changing password."
  else
    echo "Password for $other_user was changed sucessfully."
  fi
fi 

if [[ $opt == "Add user" ]]; then
  read -p "x509 authentication for new user [no/yes]: " x509_auth_newuser
  read -p "Database for new user: " dbname
  read -p "Role of new user: " newuser_role

  if [[ $x509_auth_newuser == "yes" ]]; then
    read -p "Subject DN: " subject_dn_newuser
    $connect "db.getSiblingDB(\"\$external\").runCommand({createUser: \"$subject_dn_newuser\", roles: [{role: \"$newuser_role\", db: \"$dbname\"}]})"
  else
    read -p "Username of new user: " newuser
    $connect "db.createUser({user: \"$newuser\", pwd: passwordPrompt(), roles: [{role: \"$newuser_role\", db: \"$dbname\"}]})" \
    $dbname
  fi
  status=$?
fi

if [[ $opt == "Remove user" ]]; then
  read -p "Username to remove: " other_user
  read -p "Authentication database of the user: " dbname
  output=$($connect "db.getSiblingDB(\"$dbname\").dropUser(\"$other_user\")")
  status=$?
  if [[ $status -ne 0 ]] || [[ $output == *"false"* ]]; then
    echo "Error removing user."
    echo $output
  else
    echo "User $other_user was removed."
  fi
fi

if [[ $opt == "Grant role to user" ]]; then
  read -p "Username to update: " other_user
  read -p "Authentication database of the user: " dbname
  read -p "Role to grant: " role
  read -p "Database for the granted role: " role_dbname
  output=$($connect "db.getSiblingDB(\"$dbname\").grantRolesToUser(\"$other_user\", [{role: \"$role\", db: \"$role_dbname\"}])")
  status=$?
fi

if [[ $opt == "Revoke role from user" ]]; then
  read -p "Username to update: " other_user
  read -p "Authentication database of the user: " dbname
  read -p "Role to revoke: " role
  read -p "Database for the revoked role: " role_dbname
  output=$($connect \
  "db.getSiblingDB(\"$dbname\").revokeRolesFromUser(\"$other_user\", [{role: \"$role\", db: \"$role_dbname\"}])")
  status=$?
fi

if [[ $opt == "Print database users" ]]; then
  read -p "Authentication database to print: " dbname
  $connect "db.getSiblingDB(\"$dbname\").getUsers()"
  status=$?
fi

status=$?
if [[ $status -ne 0 ]]; then
  echo Action \"$opt\" exited with error.
fi

exit $status

