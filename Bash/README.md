## About

* Bash script automating adminstrative tasks using mongo shell commands.


This code automates MongoDB administrative tasks without knowledge of Mongo shell language (JavaScript).

* Files
    - *mongo_admin.sh*: the main bash script
    - *cacert.pem*: the CA certificate
    - *README.md*: this file

## Prerequisites

MongoDB client is required to for this script to run: mongodb-client >= 4.2 


## How to use bash script 

Run the script __mongo_admin.sh__. First you will be asked to provide input for

* x509 authentication: answer with [yes|no] (default: no). If you want to
  authenticate with an x509 certificate then you should answer with "yes". The
  authentication database is set to *$external* in this case.
* Your authentication database: this is the database with the username you want
  to authenticate as administrator. If you do not have **dbAdmin**,
  **userAdmin**, **dbOwner** or **changeOwnPassword** roles you may not use the
  tool.
* Your username: this is the username in the your authentication database.

After these steps the tool will prompt the following menu:

 ```
1) Change password
2) Add user
3) Remove user
4) Grant role to user
5) Revoke role from user
6) Print users
7) Quit
```

The tasks are explained in more detail below. The session is closed after the
selected task is completed. Users and can be managed one by one. No bulk
insertions, deletions and updates are possible.

Users authenticating with with x509 certificates are on *$external* database.

## Change password

You will be prompted twice for a password: the first prompt is for your own
password. The second password prompt is for the new password for the user to
change the password.

Two use cases are covered:
* You can change your own password in this way if you have the 
    **changeOwnPassword** role. A person with **dbOwner**, **userAdmin** or
    **dbAdmin** roles can grant you **changeOwnPassword** role.
* You can reset passwords of users who have forgotten their passwords. In this
    case you must have **dbOwner**, **userAdmin** or **dbAdmin** role.

## Adding a new user

You will be prompted twice for a password: the first time is your own password.
The second password prompt is for the initial password for the new user. The
user must obtain this password from you and then change it. For this task you must have **dbOwner**, **userAdmin** or **dbAdmin** role.

## Removing a user

For this task you must have either **dbOwner**, **userAdmin** or **dbAdmin**
role.

## Granting and revoking roles

Roles can be granted and revoked one per session. The following roles can be
granted: read, readWrite, dbOwner and changeOwnPassword. For this task you must
have either **dbOwner** or **dbAdmin** role.

## Printing users

For this task you must have either **dbOwner**, **userAdmin** or **dbAdmin**
role.
