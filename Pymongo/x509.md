# How to get and use an x509 certificate

To get an x509 certificate the user should apply for a personal certificate under:
https://gridka-ca.kit.edu/ using a web browser. After retrieving the signed
certificate from the CA in your web browser, export the certificate and your
private key into a PKCS12 keystore under the filename `usercert.p12` and protect
the store with a password. For this you should use the certificate export
function of your web browser. For use with MongoDB the certificate and the key
have to be extracted from the `usercert.p12` file using the command openssl:

    umask 077
    openssl pkcs12 -clcerts -nokeys -in usercert.p12 -out usercert.pem
    openssl pkcs12 -nocerts -in usercert.p12 -out userkey.pem
    cat userkey.pem usercert.pem > usercertkey.pem

The output of this command:

    openssl x509 -inform PEM -subject -nameopt RFC2253 -noout -in usercert.pem | cut -f2- -d' '

is the Subject DN (username) to use to manage x509 users with `pymongo_admin.py`.

If you authenticate with x509 you must select $external as authentication
database and at the prompt **"Locate your certificate/key file: "** you should
specify the full path to `usercertkey.pem` as created above.

### Additional information:

Get the CA root certificate and store it under the name `cacert.pem`:

    wget http://www.gridka.de/ca/dd4b34ea.pem -O cacert.pem

FireWorks users should write a `launchpad.yaml` configuration like this (NOTE:
username and password must not be specified):

    host: scs-mongodb.scc.kit.edu
    port: 27017
    name: <your database name>
    authsource: $external
    ssl: true
    ssl_ca_certs: /patch/to/your/cacert.pem
    ssl_certfile: /path/to/your/usercert.pem
    ssl_keyfile: /path/to/your/userkey.pem
    ssl_pem_passphrase: <your PEM userkey passphrase, remove the line or set to null if none>
    mongoclient_kwargs:
      authmechanism: MONGODB-X509