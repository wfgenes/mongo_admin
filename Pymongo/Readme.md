## pymongo_admin.py

This script automates user management tasks on MongoDB instances. You can
interactively perform the following actions based on your role and permissions:

  - Create User
  - Change password
  - Remove user
  - Grant a role to user 
  - Revoke a role from user
  - Show users
  - List available databases
  - Create a database
  - Drop a database

To get a short help you can start the script with "--help" or "-h" flags:

    python pymongo_admin.py --help

The default server and CA certificate can be replaced with the ones of your
specific MongoDB server:

    python ./pymongo_admin.py --host your_server_hostname --cacert /path/to/ca/certificate.pem

If your MongoDB server requires a client certificate for the SSL connection:

    python ./pymongo_admin.py --clcert /path/to/client/certificate.pem

After starting the script and before executing any action on a database you
must select the method to authenticate ('log in') to mongodb: 

    Please enter your your login credential.
    Do you use x509 authentication [no/yes] (press Enter if you don't know): 

Depending on your answer (yes/no) there are two methods to login:

## Login with x509 certificate

If you answer with **yes** then you will get a prompt to specify the absolute
path of your certificate/key file:

    Locate your certificate/key file: <absolute address>

The user certificate/key file is not the CA certificate file `cacert.pem` that
is included in this repository.

_Please refer to [How to get and use x509 certificate](x509.md) for more information._ 

If you have set a key password, you will be additionally asked for PEM password: 

    Enter PEM pass phrase:

## Login with username and password

To login with password, you will be asked to set following fields:

    Your authentication database:
    Your username to login: 
    Your password:

After successful login using one of the authentication methods you will see
following menu: 

    1: Create User
    2: Change password
    3: Remove user
    4: Grant a role to user 
    5: Revoke a role from user
    6: Show users
    7: List available databases
    8: Create a database
    9: Drop a database
    0: Quit/Log out

    your choice (0 for Quit): 

The rest of the document goes through the whole menu step by step and cover
all cases. 

## Create User

User of the script with proper role on mongodb instance can create new users.
In most cases new users can have **read**, **readWrite**  and **dbOwner**
roles, however, for adding and running workflows you need Write permission.
Please refer to
[mongodb manual](https://docs.mongodb.com/manual/reference/built-in-roles/)
for additional information on built-in roles.


## Change Password

Changing user's password is straightforward:

    Username to update the pass:
    Authentication database of user: 

The authentication database is where the user has been created.


## Remove user

You can delete users from database by selecting **remove user** option. Authentication database is where the user generated in first place.

    Username to be removed from :
    Authentication database of user :


## Grant or revoke a role

In order to grant/revoke a role from users you should specify **authentication** and **target database**. 
 
    Authentication database:
    Data base for which new role will be granted, target database:

_The database where you create the user is authentication database. Although the user would authenticate to this database, the user can have roles in other databases (target database); i.e. the user’s authentication database does not limit the user’s privileges._


## Show users

Use this command to view all users on the specific database select show users option and provide your database name. _Please note that for executing this option you should have necessary privilege_.


## List available databases

Use this command to get an overview of the databases to which you have access.


## Create or drop a database

These commands are provided only for users with roles allowing such operations.
