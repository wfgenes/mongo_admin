## About

This code automates MongoDB administrative tasks without knowledge of mongo shell language (JavaScript).

This repository contains scripts to automate adminstrative tasks on MongoDB.

* Using PyMongo, the Python API for MongoDB
* Bash script that automates adminstrative tasks using mongo shell commands

See the README files in each folder for detailed usage instructions.
